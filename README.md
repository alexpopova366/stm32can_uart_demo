# STM32_CAN_Loopback

## Test project for STM32F446RE board

This project tests reception and transmission in Loopback mode (it transmits and receives data by itself).

![Demo](Video/out.mp4)

## Launch

To view the reception and transmission results, you need to start a serial port terminal at a baud rate of 115200:

```bash
screen /dev/ttyACM0 115200
```

To start transmission, press the blue button.

As a result, the terminal will display a message about the button press, message transmission, and message reception:

```bash
Button click #25!       Message #25 sent: "19"  ==>             Message #25 received: "19"
```
